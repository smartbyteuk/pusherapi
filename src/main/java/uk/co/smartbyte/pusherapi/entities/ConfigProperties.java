package uk.co.smartbyte.pusherapi.entities;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties
public class ConfigProperties {


    @NotBlank
    private String appID;
    @NotBlank
    private String key;
    @NotBlank
    private String secret;
    @NotBlank
    private String cluster;
    @NotBlank
    private String encryption;


    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public boolean getEncryption() {
        return encryption.equals("true") ? true : false;
    }

    public void setEncryption(String encryption) {
        this.encryption = encryption;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
