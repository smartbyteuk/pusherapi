package uk.co.smartbyte.pusherapi.entities;

public class PushEventException extends Exception{
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PushEventException{" +
                "message='" + message + '\'' +
                '}';
    }
}
