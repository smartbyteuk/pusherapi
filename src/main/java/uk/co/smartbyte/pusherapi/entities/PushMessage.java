package uk.co.smartbyte.pusherapi.entities;

import java.io.Serializable;

public class PushMessage implements Serializable {

    private static final long serialVersionUID = 881604564889989263L;

    private String channelID;

    private String event;

    private String key;

    private String message;

    public PushMessage(String channelID, String event, String key, String message) {
        this.channelID = channelID;
        this.event = event;
        this.key = key;
        this.message = message;
    }

    public PushMessage(){
        //no args constructor
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
