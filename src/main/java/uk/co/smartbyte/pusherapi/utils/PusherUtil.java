package uk.co.smartbyte.pusherapi.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pusher.rest.Pusher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.co.smartbyte.pusherapi.entities.ConfigProperties;
import uk.co.smartbyte.pusherapi.entities.PushEventException;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class PusherUtil {

    private static final Logger logger = LoggerFactory.getLogger(PusherUtil.class);

    private ConfigProperties properties;

    @Autowired
    public void setProperties(ConfigProperties properties) {
        this.properties = properties;
    }

    private static PusherUtil ourInstance = new PusherUtil();

    Pusher pusher;

    private PusherUtil() {

        //Singleton constructor - takes noargs

    }

    @PostConstruct
    public void init(){
        this.pusher = new Pusher( properties.getAppID(), properties.getKey(), properties.getSecret());
        this.pusher.setCluster(properties.getCluster());
        this.pusher.setEncrypted(properties.getEncryption());

    }

    public static PusherUtil getInstance() {
        return ourInstance;
    }

    public void send(String channelID, String event, String key, Object message) throws PushEventException {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String jsonMessage = mapper.writeValueAsString(message);
            pusher.trigger(channelID, event, Collections.singletonMap(key, jsonMessage));
            logger.debug("Pushing message " + jsonMessage);
        } catch (JsonProcessingException e) {
            logger.error("Could not send message : " + e.getMessage());
            PushEventException pushEventException = new PushEventException();
            pushEventException.setMessage("Could not send message : " + e.getMessage());
            throw pushEventException;
        }

    }
}
