package uk.co.smartbyte.pusherapi.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uk.co.smartbyte.pusherapi.entities.PushEventException;
import uk.co.smartbyte.pusherapi.entities.PushMessage;
import uk.co.smartbyte.pusherapi.utils.PusherUtil;

import javax.xml.ws.Response;

@Controller
@RequestMapping(path="/api/push")
public class PusherAPI {

    private static final Logger logger = LoggerFactory.getLogger(PusherAPI.class);

    private PusherUtil pusherUtil;

    @Autowired
    private void setPusherUtil(PusherUtil pusherUtil){
        this.pusherUtil = pusherUtil;
    }

    @GetMapping(path="health")
    public @ResponseBody String apiHealth(){
        return "Healthy";
    }

    @GetMapping(path="test")
    public @ResponseBody String test(){
        try {
            pusherUtil.send("Global", "Event", "PushMessage", "{data: 'hello'}");
        } catch (PushEventException e){
            logger.error(e.getMessage());
        }
        return "PushMessage sent";
    }

    @GetMapping(path="sampleMessage")
    public ResponseEntity<PushMessage> sampleMessage(){
        PushMessage sample = new PushMessage("Channel_value", "Event_value", "Key_value", "{data: 'value'");
        return ResponseEntity.status(HttpStatus.OK).body(sample);
    }

    @PostMapping(path="push", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> push (@RequestBody PushMessage pushMessage){
        try{
            pusherUtil.send(pushMessage.getChannelID(), pushMessage.getEvent(), pushMessage.getKey(), pushMessage.getMessage());
        } catch (PushEventException pushEventException){
            logger.error(pushEventException.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(pushEventException.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).build();

    }
}
