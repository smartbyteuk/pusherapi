# PusherAPI 
### A web based microservice for sending push messages using Pusher.com

###Features

- RESTFul HTTP interface to send messages

Requires a application.yaml file to provide pusher.com configuration information e.g.

```yaml
appID: YOUR_APP_ID
key: YOUR_APPS_KEY
secret: YOUR_SECRET
cluster: YOUR_CLUSTER
encryption: true/false
```

#### To Run

```bash
docker pull smartbyteuk/pusherapi:latest
```

```bash
docker run -it -v PATH_TO_APPLICATION.YAML:/tmp/application.yaml -p 8080:8080 smartbyteuk/pusherapi:latest
```
