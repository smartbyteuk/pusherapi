FROM anapsix/alpine-java:latest
VOLUME /tmp
COPY application.yaml /tmp/application.yaml
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar", "--spring.config.location=/tmp/application.yaml"]